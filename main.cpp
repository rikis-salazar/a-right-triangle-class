#include "ccc_win.h"
#include "right_triangle.h"

using namespace std;

// Function that tests our code.
int ccc_win_main() {
    // Draw angry face
    RightTriangle triangle1;
    triangle1.draw();

    Point center2(-5, 5);
    RightTriangle triangle2(center2);
    triangle2.draw();

    double width3 = 3.14;
    double height3 = 4.13;
    RightTriangle triangle3(width3, height3);
    triangle3.draw();

    RightTriangle triangle4(Point(-5, -3), 8.42, -2.48);
    triangle4.draw();

    RightTriangle triangle5(Point(5, -3), -8.42, -2.48);
    triangle5.draw();

    center2.move(10, 0);
    RightTriangle triangle6(center2, -2, 1);
    triangle6.draw();

    // Testing accessors (indirectly via a non-member function)
    display_triangle_info(triangle1, -6);
    display_triangle_info(triangle2, -7);
    display_triangle_info(triangle3, -8);
    display_triangle_info(triangle6, -9);

    // Testing setters
    triangle1.set_main_corner( Point(-8, 0) );
    triangle1.set_width( triangle1.get_height() );
    triangle1.set_height( -2 * triangle1.get_width() );
    triangle1.draw();
    display_triangle_info(triangle1, 9);

    // Testing miscellaneous functions
    triangle1.draw_reflection();

    triangle1.translate_to(8,0);
    triangle1.draw();

    for ( int i = 0 ; i < 3 ; i++ ){
        triangle1.rotate_clockwise();
        triangle1.draw();
    }

    triangle1.move(-10,0);
    triangle1.draw();

    Point p = triangle3.get_main_corner();
    p.move(-6,-1);
    if ( triangle3 >= triangle2 )
        cwin << Message( p, "Triangle 3 is bigger than triangle 2" );

    p.move(0, -1);
    if ( triangle4 != triangle6 )
        cwin << Message( p, "Triangles 4 & 6 have different areas" );

    return 0;
}
