#include "right_triangle.h"
#include "ccc_win.h"

using namespace std;

// Member function: Default constructor
RightTriangle::RightTriangle(){
    main_corner = Point(0, 0);
    width = 2.0;
    height = 1.0;
}

// Member function: One-parameter constructor (constructor overload)
RightTriangle::RightTriangle( Point c ){
    main_corner = c;
    width = 2.0;
    height = 1.0;
}

// Member function: Two-parameter constructor (constructor overload)
RightTriangle::RightTriangle( double w, double h ){
    main_corner = Point(0, 0);
    width = w;
    height = h;
}

// Member function: Three-parameter constructor (constructor overload)
RightTriangle::RightTriangle(Point c, double w, double h){
    main_corner = c;
    width = w;
    height = h;
}


// ***** GETTERS *****
Point RightTriangle::get_main_corner() const {
    return main_corner;
}

double RightTriangle::get_width() const {
    return width;
}

double RightTriangle::get_height() const {
    return height;
}

double RightTriangle::get_area() const {
    double signed_area = (width * height) / 2;
    if (signed_area >= 0)
        return signed_area;
    else
        return -signed_area;
}


// ***** MUTATORS *****
void RightTriangle::set_main_corner( Point c ){
    main_corner = c;
    only_works_inside_RightTriangle();
}
void RightTriangle::set_width( double w ){
    width = w;
}
void RightTriangle::set_height( double h ){
    height = h;
}


// ***** MISCELLANEOUS FUNCTIONS *****
//
// Draws the triangle
void RightTriangle::draw() const {
    Point aux1 = main_corner;
    Point aux2 = main_corner;

    aux1.move(width, 0);
    aux2.move(0, height);

    cwin << Line(main_corner, aux1);
    cwin << Line(aux1, aux2);
    cwin << Line(aux2, main_corner);
}

// Alters width and height but does not redraw the triangle.
void RightTriangle::rotate_clockwise(){
    // Clockwise rotation:
    //     (x,y) --> (y,-x)
    double x = width;
    double y = height;
    width = y;
    height = -x;
}

// Draws a reflected triangle, but the original triangle remains unchanged.
void RightTriangle::draw_reflection() const {
    // Reflection w.r.t origin:
    //     (x,y) --> (-x,-y)
    double x = width;
    double y = height;
    RightTriangle reflected_local_triangle(main_corner, -x, -y);
    reflected_local_triangle.draw();
}

// Moves the triangle by the specified increments.
void RightTriangle::move( double dx, double dy ){
    main_corner.move(dx, dy);
}

// Moves the triangle to its new location.
void RightTriangle::translate_to( double x, double y ){
    main_corner = Point(x, y);
}


// ***** BOOLEAN OPERATORS *****
//
bool RightTriangle::operator<( const RightTriangle& rhs ) const {
    return get_area() < rhs.get_area();
}

bool RightTriangle::operator<=( const RightTriangle& rhs ) const {
    return get_area() <= rhs.get_area();
}

bool RightTriangle::operator>( const RightTriangle& rhs ) const {
    return get_area() > rhs.get_area();
}

bool RightTriangle::operator>=( const RightTriangle& rhs ) const {
    return get_area() >= rhs.get_area();
}

bool RightTriangle::operator==( const RightTriangle& rhs ) const {
    return get_area() == rhs.get_area();
}

bool RightTriangle::operator!=( const RightTriangle& rhs ) const {
    return get_area() != rhs.get_area();
}


// ***** PRIVATE HELPERS *****
void RightTriangle::only_works_inside_RightTriangle() const {
    // Draws a little circle around the main corner of a triangle
    cwin << Circle(main_corner, 0.5);
}




// ***** NON-MEMBERS *****
void display_triangle_info( const RightTriangle &t, double y_coord ) {
    cwin << Message(Point(-9.5, y_coord), "Info [x y w h a]:");
    cwin << Message(Point(-4, y_coord), t.get_main_corner().get_x());
    cwin << Message(Point(-1.5, y_coord), t.get_main_corner().get_y());
    cwin << Message(Point(1, y_coord), t.get_width());
    cwin << Message(Point(3.5, y_coord), t.get_height());
    cwin << Message(Point(6, y_coord), t.get_area());
}
