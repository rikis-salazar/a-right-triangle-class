# Learning to write `C++` classes using graphics

A series of commits that illustrate how to code a `RightTriangle` class in
`C++`.

> Note: The `ccc_*` files are needed to compile this example.
